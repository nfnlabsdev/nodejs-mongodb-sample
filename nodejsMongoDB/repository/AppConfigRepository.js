module.exports = function () {
  const appConfig = 'AppConfig'
  const slider = 'AppSlider'
  const emailTemplate = 'EmailTemplate'
  const MongoClient = require('mongodb').MongoClient
  require('dotenv').config()

  const url = 'mongodb://localhost:27017'

  const mycol = 'mycol'

  // const config = {
  //   client: 'mysql2',
  //   connection: {
  //     host: process.env.DB_HOST,
  //     user: process.env.DB_USER,
  //     password: process.env.DB_PASS,
  //     database: process.env.DB_NAME
  //   },
  //   pool: {
  //     min: Number(process.env.DB_POOL_MIN),
  //     max: Number(process.env.DB_POOL_MAX)
  //   },
  //   acquireConnectionTimeout: Number(process.env.DB_TIMEOUT)
  // }
  // const Knex = require('knex')

  this.insertData = (data) => {
    var response = {}
    return new Promise(function (resolve) {
      MongoClient.connect(url, function (err, client) {
        let db = client.db('test')
        db.collection(mycol).insert(data, function (err, res) {
          if (err) {
            response.error = true
            response.msg = 'FAILED'
          } else {
            response.error = false
            response.msg = 'INSERTED'
          }
          resolve(response)
          client.close()
        })
      })
    })
  }

  this.selectData = (data, callback) => {
    var response = {}
    MongoClient.connect(url, function (err, db) {
      var cursor = db.collection(mycol).find()
      cursor.each(function (err, doc) {
        console.log(doc)
        callback(doc)
      })
    }) 
  }

  this.updateData = (data) => {
    var response = {}
    return new Promise(function (resolve) {

    })
  }

  this.removeData = (data) => {
    var response = {}
    return new Promise(function (resolve) {

    })
  }

  // this.authTypeCheck = (data) => {
  //   var response = {}
  //   return new Promise(function (resolve) {
  //     var knex = new Knex(config)
  //     knex(appConfig)
  //       .select('Value', 'FieldName')
  //       .where(data.type)
  //       .where(data.fieldname)
  //       .then((result) => {
  //         if (result.length > 0) {
  //           response.error = false
  //           response.result = result[0]
  //         } else {
  //           response.error = true
  //           response.result = null
  //         }
  //         resolve(response)
  //       })
  //       .catch((err) => {
  //         err.error = true
  //         err.result = null
  //         resolve(response)
  //       })
  //       .finally(() => {
  //         knex.destroy()
  //       })
  //   })
  // }

  // this.fetchSlider = (data, callback) => {
  //   var response = {}
  //   var knex = new Knex(config)
  //   knex(slider)
  //     .select('Id as id', 'Title as title', 'Description as description', 'Image as image')
  //     .where(data)
  //     .then((result) => {
  //       if (result.length > 0) {
  //         response.error = false
  //         response.result = result
  //       } else {
  //         response.error = true
  //       }
  //       callback(response)
  //     })
  //     .catch((err) => {
  //       err.error = false
  //       callback(err)
  //     })
  //     .finally(() => {
  //       knex.destroy()
  //     })
  // }

  // this.fetchEmailTemplate = (id, knex) => {
  //   var response = {}
  //   return new Promise(function (resolve) {
  //     var knex = new Knex(config)
  //     knex(emailTemplate)
  //       .where('id', id)
  //       .then((result) => {
  //         if (result.length > 0) {
  //           response.error = false
  //           response.result = result
  //         } else {
  //           response.error = true
  //         }
  //         resolve(response)
  //       })
  //       .catch((err) => {
  //         err.error = true
  //         resolve(err)
  //       })
  //       .finally(() => {
  //         knex.destroy()
  //       })
  //   })
  // }
}
