module.exports = function (server, validator) {
  const basePath = '/api/'
  require('../controller/UserController')()
  require('../Utils/error')()

  server.get(basePath + 'config', (request, response) => {
    this.appSetting((result) => {
      const lang = request.headers.lang
      this.ctrlHandler([result], result.error, lang, (message) => {
        return response.send(message)
      })
    })
  })

  server.post(basePath + 'check', [
    validator.check('mobile').isLength({ min: 8, max: 15 }).withMessage('NUMERIC_LIMIT: $[1] $[2] $[3],mobile,8,15')
      .isNumeric().withMessage('NUMERIC: $[1], mobile'),
    validator.check('countryCode')
      .isLength({ min: 1, max: 5 }).withMessage('NUMERIC_LIMIT: $[1] $[2] $[3],Country Code, 1, 5')
      .isNumeric().withMessage('NUMERIC: $[1], mobile')
  ], (request, response) => {
    const error = validator.validation(request)
    const lang = request.headers.lang
    if (error.array().length) {
      this.requestHandler(error.array(), true, lang, (message) => {
        return response.send(message)
      })
    } else {
      var body = request.body
      var mobile = {}
      mobile.number = body.mobile
      mobile.ext = body.countryCode
      this.mobileValidation(mobile, (result) => {
        this.ctrlHandler([result], result.error, lang, (message) => {
          return response.send(message)
        })
      })
    }
  })

  server.post(basePath + 'signup', [
    validator.check('email')
      .isEmail().withMessage('INVALID: $[1], email Id'),
    validator.check('firstName')
      .isLength({ min: 1, max: 255 }).withMessage('TEXT_LIMIT: $[1] $[2] $[3],firstname,1,50'),
    validator.check('lastName')
      .isLength({ min: 1, max: 255 }).withMessage('TEXT_LIMIT: $[1] $[2] $[3],lastname,1,50')
  ], (request, response) => {
    const error = validator.validation(request)
    const lang = request.headers.lang
    if (error.array().length) {
      this.requestHandler(error.array(), true, lang, (message) => {
        return response.send(message)
      })
    } else {
      var body = request.body
      this.registerUser(body, (result) => {
        this.ctrlHandler([result], result.error, lang, (message) => {
          return response.send(message)
        })
      })
    }
  })

  server.post(basePath + 'otpVerify', [
    validator.check('mobile')
      .isLength({ min: 8, max: 15 }).withMessage('NUMERIC_LIMIT: $[1] $[2] $[3],mobile,8,15')
      .isNumeric().withMessage('NUMERIC: $[1], mobile'),
    validator.check('otp')
      .isLength({ min: 4, max: 4 }).withMessage('TEXT_LIMIT: $[1] $[2] $[3],otp,1,4')
      .isNumeric().withMessage('NUMERIC: $[1], mobile'),
    validator.check('countryCode')
      .isLength({ min: 2, max: 5 }).withMessage('TEXT_LIMIT: $[1] $[2] $[3],countryCode,1,4'),
    validator.check('type')
      .isLength({ min: 5, max: 10 }).withMessage('TEXT_LIMIT: $[1] $[2] $[3],type,5,10')
      .isString().withMessage('TEXT_LIMIT: $[1] $[2] $[3],type,6,10'),
    validator.check('uuid')
      .isLength({ min: 10, max: 50 }).withMessage('TEXT_LIMIT: $[1] $[2] $[3],uuid,10,50'),
    validator.check('languageName')
      .optional()
      .isLength({ min: 0, max: 10 }).withMessage('TEXT_LIMIT: $[1] $[2] $[3],languageName,0,10')
  ], (request, response) => {
    const error = validator.validation(request)
    const lang = request.headers.lang
    if (error.array().length) {
      this.requestHandler(error.array(), true, lang, (message) => {
        return response.send(message)
      })
    } else {
      var body = request.body
      this.otpValidate(body, (result) => {
        this.ctrlHandler([result], result.error, lang, (message) => {
          return response.send(message)
        })
      })
    }
  })

  server.post(basePath + 'resendOtp', [
    validator.check('mobile')
      .isLength({ min: 8, max: 15 }).withMessage('NUMERIC_LIMIT: $[1] $[2] $[3],mobile, 8, 15')
      .isNumeric().withMessage('NUMERIC: $[1], mobile'),
    validator.check('countryCode')
      .isLength({ min: 2, max: 5 }).withMessage('TEXT_LIMIT: $[1] $[2] $[3],countryCode,1,4'),
    validator.check('type')
      .isLength({ min: 5, max: 10 }).withMessage('TEXT_LIMIT: $[1] $[2] $[3],type,5,10')
      .isString().withMessage('TEXT_LIMIT: $[1] $[2] $[3],type,6,10')
  ], (request, response) => {
    const error = validator.validation(request)
    const lang = request.headers.lang
    if (error.array().length) {
      this.requestHandler(error.array(), true, lang, (message) => {
        return response.send(message)
      })
    } else {
      var body = request.body
      this.recallOTP(body, (result) => {
        this.ctrlHandler([result], result.error, lang, (message) => {
          return response.send(message)
        })
      })
    }
  })

  server.post(basePath + 'pwdVerify', [
    validator.check('mobile')
      .isLength({ min: 8, max: 15 }).withMessage('NUMERIC_LIMIT: $[1] $[2] $[3],mobile, 8, 15')
      .isNumeric().withMessage('NUMERIC: $[1], mobile'),
    validator.check('countryCode')
      .isLength({ min: 2, max: 5 }).withMessage('TEXT_LIMIT: $[1] $[2] $[3],countryCode,1,4'),
    validator.check('password')
      .isLength({ min: 8, max: 20 }).withMessage('TEXT_LIMIT: $[1] $[2] $[3],password, 8, 20'),
    validator.check('uuid')
      .isLength({ min: 10, max: 255 }).withMessage('TEXT_LIMIT: $[1] $[2] $[3],uuid, 10, 255')
  ], (request, response) => {
    const error = validator.validation(request)
    const lang = request.headers.lang
    if (error.array().length) {
      this.requestHandler(error.array(), true, lang, (message) => {
        return response.send(message)
      })
    } else {
      var body = request.body
      this.pwdValidate(body, (result) => {
        this.ctrlHandler([result], result.error, lang, (message) => {
          return response.send(message)
        })
      })
    }
  })

  server.post(basePath + 'forgotPwdOtp', [
    validator.check('mobile')
      .isLength({ min: 8, max: 15 }).withMessage('NUMERIC_LIMIT: $[1] $[2] $[3],mobile, 8, 15')
      .isNumeric().withMessage('NUMERIC: $[1], mobile'),
    validator.check('countryCode')
      .isLength({ min: 2, max: 5 }).withMessage('TEXT_LIMIT: $[1] $[2] $[3],countryCode,1,4')
  ], function (request, response) {
    const error = validator.validation(request)
    const lang = request.headers.lang
    if (error.array().length) {
      this.requestHandler(error.array(), true, lang, (message) => {
        return response.send(message)
      })
    } else {
      var body = request.body
      this.forgotPwdOtp(body, (result) => {
        this.ctrlHandler([result], result.error, lang, (message) => {
          return response.send(message)
        })
      })
    }
  })

  server.post(basePath + 'updatePwd', [
    validator.check('mobile')
      .isLength({ min: 8, max: 15 }).withMessage('NUMERIC_LIMIT: $[1] $[2] $[3],mobile, 8, 15')
      .isNumeric().withMessage('NUMERIC: $[1], mobile'),
    validator.check('countryCode')
      .isLength({ min: 2, max: 5 }).withMessage('TEXT_LIMIT: $[1] $[2] $[3],countryCode,1,4'),
    validator.check('otp')
      .isLength({ min: 4, max: 4 }).withMessage('OTP'),
    validator.check('password')
      .isLength({ min: 8, max: 14 }).withMessage('PASSWORD: $[1] $[2],8,14')
  ], function (request, response) {
    const error = validator.validation(request)
    const lang = request.headers.lang
    if (error.array().length) {
      this.requestHandler(error.array(), true, lang, (message) => {
        return response.send(message)
      })
    } else {
      var body = request.body
      this.UpdatePwd(body, (result) => {
        this.ctrlHandler([result], result.error, lang, (message) => {
          return response.send(message)
        })
      })
    }
  })

}
